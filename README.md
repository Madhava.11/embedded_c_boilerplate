# Embedded c_files_template

Templates for Embedded C programming language source (.c) and header (.h) files

# How to add to Eclipse-based softwares

Go to "Window -> Preferences -> C/C++ -> Code Style -> Code Templates -> Files -> C Source File" and click New. Write a name for the new template. In the Pattern section copy the contents of template.c file. Click apply.
For the C Header File copy the template.h file in the Pattern section.

When you create a new file for a project, choose your own template.

## Overview

Embedded C is basically an extension to the Standard C Programming Language with additional features like Addressing I/O, multiple memory addressing and fixed-point arithmetic, etc.

The following part shows the basic structure of an Embedded C Program.

- Multiline Comments . . . . . Denoted using /*……*/
- Single Line Comments . . . . . Denoted using //
- Preprocessor Directives . . . . . #include<…> or #define
- Global Variables . . . . . Accessible anywhere in the program
- Function Declarations . . . . . Declaring Function
- Main Function . . . . . Main Function, execution begins here
```
{
Local Variables . . . . . Variables confined to main function
Function Calls . . . . . Calling other Functions
Infinite Loop . . . . . Like while(1) or for(;;)
Statements . . . . .
....
....
}
```
- Function Definitions . . . . . Defining the Functions
```
{
Local Variables . . . . . Local Variables confined to this Function
Statements . . . . .
....
....
}
```
- Function Definitions . . . . . Defining the Functions
```
{
Local Variables . . . . . Local Variables confined to this Function
Statements . . . . .
....
....
}
```

## Global Variables

Global Variables, as the name suggests, are Global to the program i.e., they can be accessed anywhere in the program.

## Local Variables

Local Variables, in contrast to Global Variables, are confined to their respective function.

## Main Function

Every C or Embedded C Program has one main function, from where the execution of the program begins.

# General Rules

## Line Width

### Rules:

The width of all lines in a program shall be limited to a maximum of 80 characters.

### Reasoning:

From time-to-time, peer reviews and other code examinations are conducted on printed pages. To be useful, such print-outs must be free of distracting line wraps as well as missing (i.e., past the right margin) characters. Line width rules also ease on-screen side-by-side code differencing.

## Braces

### Rules:

- Braces shall always surround the blocks of code (a.k.a., compound
statements), following if , else , switch , while , do , and for statements; single statements and empty statements following these keywords shall also always be surrounded by braces.

- Each left brace ( { ) shall appear by itself on the line below the start of the block it opens. The corresponding right brace ( } ) shall appear by itself in the same
position the appropriate number of lines later in the file.

	**Example:**
```
{
if (depth_in_ft > 10) dive_stage = DIVE_DEEP;
// This is legal...
else if (depth_in_ft > 0)
dive_stage = DIVE_SHALLOW;
// ... as is this.
else
{
// But using braces is always safer.
dive_stage = DIVE_SURFACE;
}
}
```

### Reasoning

There is considerable risk associated with the presence of empty
statements and single statements that are not surrounded by braces. Code
constructs like this are often associated with bugs when nearby code is changed or
commented out. This risk is entirely eliminated by the consistent use of braces. The
placement of the left brace on the following line allows for easy visual checking for
the corresponding right brace.

## Common Abbreviations

### Rules

- Abbreviations and acronyms should generally be avoided unless their
meanings are widely and consistently understood in the engineering community.

- A table of project-specific abbreviations and acronyms shall be maintained in a version-controlled document.

**Example:** Appendix A contains a sample table of abbreviations and their meanings.

### Reasoning

Programmers too readily use cryptic abbreviations and acronyms in their code (and in their resumes!). Just because you know what ZYZGXL means today doesn’t mean the programmer(s) who have to read/maintain/port your code will later be able to make sense of your cryptic names that reference it.

## Keywords to Avoid

### Rules

- The auto keyword shall not be used.
- The register keyword shall not be used.
- It is a preferred practice to avoid all use of the goto keyword. If goto is used it shall only jump to a label declared later in the same or an enclosing block.
- It is a preferred practice to avoid all use of the continue keyword.

### Reasoning

The auto keyword is an unnecessary historical feature of the language.
The register keyword presumes the programmer is smarter than the compiler.
There is no compelling reason to use either of these keywords in modern
programming practice.

The keywords go to and continue still serve purposes in the language, but their use too often results in spaghetti code. In particular, the use of goto to make jumps orthogonal to the ordinary control flows of the structured programming paradigm is problematic. The occasional use of goto to handle an exceptional circumstance is acceptable if it simplifies and clarifies the code.

## Keywords

### Rules

- The static keyword shall be used to declare all functions and
variables that do not need to be visible outside of the module in which they are declared.

- The const keyword shall be used whenever appropriate. Examples include:
   - To declare a global variable accessible (by current use or scope) by any interrupt service routine,
   - To declare a global variable accessible (by current use or scope) by two or more threads,
   - To declare a pointer to a memory-mapped I/O peripheral register set (e.g., timer_t volatile * const p_timer )
   - To declare a delay loop counter.

**Example:**

```
typedef struct
{
uint16_t count;
uint16_t max_count;
uint16_t const _unused;
uint16_t control;
// read-only register
} timer_reg_t;
timer_reg_t volatile * const p_timer = (timer_reg_t *) HW_TIMER_ADDR;
```

### Reasoning

C’s static keyword has several meanings. At the module-level, global
variables and functions declared static are protected from external use. Heavy-handed use of static in this way thus decreases coupling between modules. The const and volatile keywords are even more important. The upside of using const as much as possible is compiler-enforced protection from unintended writes to data that should be read-only. Proper use of volatile eliminates a whole class of difficult-to-detect bugs by preventing compiler optimizations that would eliminate requested reads or writes to variables or registers.

# Comment Rules

## Acceptable Formats

### Rules

-  Single-line comments in the C++ style (i.e., preceded by // ) are a useful and acceptable alternative to traditional C style comments (i.e., /* ... */ ).

- Comments shall never contain the preprocessor tokens   /*, //, or \ .

-  Code shall never be commented out, even temporarily.
   - To temporarily disable a block of code, use the preprocessor
conditional compilation feature (e.g., #if 0 ... #endif ).
   - Any line or block of code that exists specifically to increase the level of debug output information shall be surrounded by #ifndef NDEBUG ...#endif .

**Example:**
```
/* The following code was meant to be part of the build...
...
safety_checker();
...
/* ... but an end of comment character sequence was omitted. */
```

### Reasoning

Whether intentional or not, nested comments run the risk of confusing
source code reviewers about the chunks of the code that will be compiled and run. Our choice of negative-logic NDEBUG is deliberate, as that constant is also associated with disabling the assert() macro. In both cases, the programmer acts to disable
the verbose code.

## Locations and Content

### Rules

- All comments shall be written in clear and complete sentences, with proper
spelling and grammar and appropriate punctuation.

- The most useful comments generally precede a block of code that performs
one step of a larger algorithm. A blank line shall follow each such code block. The comments in front of the block should be at the same indentation level.

- Avoid explaining the obvious. Assume the reader knows the C programming
language. For example, end-of-line comments should only be used where the
meaning of that one line of code may be unclear from the variable and
function names and operations alone but where a short comment makes it
clear. Specifically, avoid writing unhelpful and redundant comments, e.g.,
```
“ numero <<= 2;
// Shift numero left 2 bits. ”.
```

- The number and length of individual comment blocks shall be proportional to
the complexity of the code they describe.

- Whenever an algorithm or technical detail is defined in an external
reference—e.g., a design specification, patent, or textbook—a comment shall
include a sufficient reference to the original source to allow a reader of the
code to locate the document.

- Whenever a flow chart or other diagram is needed to sufficiently document
the code, the drawing shall be maintained with the source code under version
control and the comments should reference the diagram by file name or title.

- All assumptions shall be spelled out in comments.

- Each module and function shall be commented in a manner suitable for
automatic documentation generation, Use the following capitalized comment markers to highlight important issues:
  - “ WARNING: ” alerts a maintainer there is risk in changing this code. For example, that a delay loop counter’s terminal value was determined empirically and may need to change when the code is ported or the optimization level tweaked.
  -  “ NOTE: ” provides descriptive comments about the “why” of a chunk of code—as distinguished from the “how” usually placed in comments.For example, that a chunk of driver code deviates from the datasheet because there was an errata in the chip. Or that an assumption is being made by the original programmer.
  - “ TODO: ” indicates an area of the code is still under construction and explains what remains to be done. When appropriate, an all-caps programmer name or set of initials may be included before the word.

**Example:**
```
// Step 1: Batten down the hatches.
for (int hatch = 0; hatch < NUM_HATCHES; hatch++)
{
if (hatch_is_open(hatches[hatch]))
{
hatch_close(hatches[hatch]);
}
}
// Step 2: Raise the mizzenmast.
// TODO: Define mizzenmast driver API.
```

### Reasoning

Following these rules results in good comments. And good comments
correlate with good code. It is a best practice to write the comments before writing the code that implements the behaviors those comments outline.
Unfortunately, it is easy for source code and documentation to drift over time.
The best way to prevent this is to keep the documentation as close to the code as possible. Likewise, anytime a question is asked about a section of the code that was previously thought to be clear, you should add a comment addressing that issue nearby.

However, comments are also still necessary inside the function bodies to reduce the cost of code maintenance.

# White Space Rules

## Spaces

### Rules

- Each of the keywords if , while , for , switch , and return shall be followed
by one space when there is additional program text on the same line.

- Each of the assignment operators = , += , -= , *= , /= , %= , &= , |= , ^= , ~= , and !=
shall always be preceded and followed by one space.

- Each of the binary operators + , - , * , / , % , < , <= , > , >= , == , != , << , >> , & , | , ^ , && ,and || shall always be preceded and followed by one space.

- Each of the unary operators + , - , ++ , -- , ! , and ~ , shall be written without a
space on the operand side.

- The pointer operators * and & shall be written with white space on each side
within declarations but otherwise without a space on the operand side.

- The ? and : characters that comprise the ternary operator shall each always
be preceded and followed by one space.

- The structure pointer and structure member operators ( -> and . , respectively)
shall always be without surrounding spaces.

- The left and right brackets of the array subscript operator ( [ and ] ) shall be
without surrounding spaces, except as required by another white space rule.

- Expressions within parentheses shall always have no spaces adjacent to the
left and right parenthesis characters.

- The left and right parentheses of the function call operator shall always be
without surrounding spaces, except that the function declaration shall feature
one space between the function name and the left parenthesis to allow that
one particular mention of the function name to be easily located.

- Except when at the end of a line, each comma separating function parameters
shall always be followed by one space.

- Each semicolon separating the elements of a for statement shall always be
followed by one space.

- Each semicolon shall follow the statement it terminates without a preceding
space.

### Reasoning

In source code, the placement of white space is as important as the
placement of text. Good use of white space reduces eye strain and increases the
ability of programmers and reviewers of the code to spot potential bugs.
Enforcement: These rules shall be followed by programmers as they work as well as
reinforced via a code beautifier, e.g., GNU Indent.

## Alignment

### Rules

- The names of variables within a series of declarations shall have their first
characters aligned.

- The names of struct and union members shall have their first characters
aligned.

- The assignment operators within a block of adjacent assignment statements
shall be aligned.

- The # in a preprocessor directive shall always be located at the start of a line,
though the directives themselves may be indented within a #if or #ifdef
sequence.

**Example:**
```
#ifdef USE_UNICODE_STRINGS
#
define BUFFER_BYTES
128
#else
#
define BUFFER_BYTES
64
#endif
...
typedef struct
{
uint8_t buffer[BUFFER_BYTES];
uint8_t checksum;
} string_t;

```

### Reasoning

Visual alignment emphasizes similarity. A series of consecutive lines
each containing a variable declaration is easily seen and understood as a block of
related lines of code. Blank lines and differing alignments should be used as
appropriate to visually separate and distinguish unrelated blocks of code that
happen to be located in proximity.

## Blank Lines

### Rules

- No line of code shall contain more than one statement.

- There shall be a blank line before and after each natural block of code.
Examples of natural blocks of code are loops, if...else and switch
statements, and consecutive declarations.

- Each source file shall terminate with a comment marking the end of file
followed by a blank line.

### Reasoning

Appropriate placement of white space provides visual separation and
thus makes code easier to read and understand, just as the white space areas
between paragraphs of this coding standard aid readability. Clearly marking the
end of a file is important for human reviewers looking at printouts and the blank
line following may be required by some older compilers.

## Indentation

### Rules

- Each indentation level should align at a multiple of 4 characters from the start
of the line.

- Within a switch statement, the case labels shall be aligned; the contents of
each case block shall be indented once from there.

-  Whenever a line of code is too long to fit within the maximum line width,
indent the second and any subsequent lines in the most readable manner
possible.

**Example:**
```
sys_error_handler(int err)
{
switch (err)
{
case ERR_THE_FIRST:
...
break;
default:
...
break;
}
// Purposefully misaligned indentation; see why?
if ((first_very_long_comparison_here
&& second_very_long_comparison_here)
|| third_very_long_comparison_here)
{
...
}
}

```

### Reasoning

Fewer indentation spaces increase the risk of visual confusion while
more spaces increases the likelihood of line wraps.
Enforcement: A tool, such as a code beautifier, shall be available to programmers to
convert indentations of other sizes in an automated manner. This tool shall be used
on all new or modified files prior to each build.

## Tabs

### Rules

The tab character (ASCII 0x09) shall never appear within any source code file.

**Example:**

```
// When tabs are needed inside a string, use the ‘\t’ character.
#define COPYRIGHT
“Copyright (c) 2018 Barr Group.\tAll rights reserved.”
// When indents are needed in the source code, align via spaces instead.
void
main (void)
{
// If not, you can encounter
// all sorts
// of weird and
// uneven
// alignment of code and comments... across tools.
}
```

### Reasoning

 The width of the tab character varies by text editor and programmer
preference, making consistent visual layout a continual source of headaches during
code reviews and maintenance.

# Module Rules

## Naming Conventions

### Rules

- All module names shall consist entirely of lowercase letters,numbers, and underscores. No spaces shall appear within the module’s header and source file names.

- All module names shall be unique in their first 8 characters and end with
suffices .h and .c for the header and source file names, respectively.

- No module’s header file name shall share the name of a header file from the C Standard Library or C++ Standard Library. 

  For example,
  modules shall not be named “ stdio ” or “ math ”.

- Any module containing a main() function shall have the word “ main ” as part
of its source file name.

### Reasoning

Multi-platform development environments (e.g., Unix and Windows)
are the norm rather than the exception. Mixed case names can cause problems
across operating systems and are also error prone due to the possibility of similarly-
named but differently-capitalized files becoming confused by human programmers.
The inclusion of “main” in a filename is an aid to code maintainers that has
proven useful in projects with multiple software configurations.

## Header Files

### Rules

- There shall always be precisely one header file for each source file and they
shall always have the same root name.

- Each header file shall contain a preprocessor guard against multiple inclusion,
as shown in the example below.

- The header file shall identify only the procedures, constants, and data types
(via prototypes or macros, #define , and typedefs, respectively) about which
it is strictly necessary for other modules to be informed.

  - It is a preferred practice that no variable ever be declared (via extern )
in a header file.
  - No storage for any variable shall be allocated in a header file.

- No public header file shall contain a #include any private header file.

**Example:**
```
#ifndef ADC_H
#define ADC_H
...
#endif /* ADC_H */
```

### Reasoning:

The C language standard gives all variables and functions global scope
by default. The downside of this is unnecessary (and dangerous) coupling between
modules. To reduce inter-module coupling, keep as many procedures, constants,
data types, and variables as possible privately hidden within a module’s source file.

## Source Files

### Rules

- Each source file shall include only the behaviors appropriate to control one
“entity”. Examples of entities include encapsulated data types, active objects,
peripheral drivers (e.g., for a UART), and communication protocols or layers
(e.g., ARP).

- Each source file shall be comprised of some or all of the following sections, in
the order listed: comment block; include statements; data type, constant, and
macro definitions; static data declarations; private function prototypes; public
function bodies; then private function bodies.

- Each source file shall always #include the header file of the same
name (e.g., file adc.c should #include “adc.h” ), to allow the
compiler to confirm that each public function and its prototype match.

- Absolute paths shall not be used to include file names.

- Each source file shall be free of unused include files.

- No source file shall #include another source file.

### Reasoning

The purpose and internal layout of a source file module should be clear
to all who maintain it. For example, the public functions are generally of most
interest and thus appear ahead of the private functions they call. Of critical
importance is that every function declaration be matched by the compiler against its
prototype.

## File Templates

### Rules

A set of templates for header files and source files shall be maintained at the
project level.

### Reasoning

Starting each new file from a template ensures consistency in file header
comment blocks and ensures inclusion of appropriate copyright notices.

# Data Type Rules

## Naming Conventions

### Rules

- The names of all new data types, including structures, unions, and
enumerations, shall consist only of lowercase characters and internal
underscores and end with ‘ _t ’.

- All new structures, unions, and enumerations shall be named via a typedef .

- The name of all public data types shall be prefixed with their module name
and an underscore.

**Example:**
```
typedef struct
{
uint16_t count;
uint16_t max_count;
uint16_t _unused;
uint16_t control;
} timer_reg_t;
```

### Reasoning

Data type names and variable names are often appropriately similar.
For example, a set of timer control registers in a peripheral calls out to be named
‘ timer_reg ’. To distinguish the structure definition that defines the register layout,
it is valuable to create a new type with a distinct name, such as ‘ timer_reg_t ’. If
necessary this same type could then be used to create a shadow copy of the timer
registers, say called ‘ timer_reg_shadow ’.

## Fixed-Width Integers

### Rules
- Whenever the width, in bits or bytes, of an integer value matters in
the program, one of the fixed width data types shall be used in place
of char , short , int , long , or long long .

Integer Width Signed Type Unsigned Type

8 bits int8_t uint8_t 

16 bits int16_t uint16_t

32 bits int32_t uint32_t

64 bits int64_t uint64_t

- The keywords short and long shall not be used.

- Use of the keyword char shall be restricted to the declaration of and
operations concerning strings.

### Reasoning

The C90 standard purposefully allowed for implementation-defined
widths for char , short , int , long , and long long types, which has resulted in code
portability problems. The C99 standard did not resolve this but did introduce the
type names shown in the table, which are defined in the stdint.h header file.

## Signed and Unsigned Integers

### Rules

- Bit-fields shall not be defined within signed integer types.

-  None of the bitwise operators (i.e., & , | , ~ , ^ , << , and >> ) shall be
used to manipulate signed integer data.

- Signed integers shall not be combined with unsigned integers in
comparisons or expressions. In support of this, decimal constants
meant to be unsigned should be declared with a ‘ u ’ at the end.

**Example:**
```
uint16_t unsigned_a = 6u;
int16_t signed_b
= -9;
if (unsigned_a + signed_b < 4)
{
// Execution of this block appears reliably logical, as -9 + 6 is -3
...
}
// ... but compilers with 16-bit int may legally perform (0xFFFF – 9) + 6.
```

### Reasoning

Several details of the manipulation of binary data within signed integer
containers are implementation-defined behaviors of the ISO C standards.
Additionally, the results of mixing signed and unsigned integers can lead to data-
dependent outcomes like the one in the code above. Beware that the use of C99’s

## Floating Point

### Rules

- Avoid the use of floating point constants and variables whenever possible.
Fixed-point math may be an alternative.

- When floating point calculations are necessary:

  - Use the C99 type names float32_t , float64_t , and float128_t .
  - Append an ‘ f ’ to all single-precision constants (e.g., pi = 3.141592f ).
  - Ensure that the compiler supports double precision, if your math
depends on it.
  - Never test for equality or inequality of floating point values.
  - Always invoke the isfinite() macro to check that prior calculations
have resulted in neither INFINITY nor NAN .

**Example:**
```
#include <limits.h>
#if (DBL_DIG < 10)
#
// Ensure the compiler supports double precision.
error “Double precision is not available!”
#endif
```
### Reasoning

A large number of risks of defects stem from incorrect use of floating
point arithmetic. By default, C promotes all floating-point constants to double
precision, which may be inefficient or unsupported on the target platform.
However, many microcontrollers do not have any hardware support for floating
point math. The compiler may not warn of these incompatibilities, instead
performing the requested numerical operations by linking in a large (typically a few
kilobytes of code) and slow (numerous instruction cycles per operation) floating-
point emulation library.

## Structures and Unions

### Rules

- Appropriate care shall be taken to prevent the compiler from
inserting padding bytes within struct or union types used to
communicate to or from a peripheral or over a bus or network to
another processor.

- Appropriate care shall be taken to prevent the compiler from
altering the intended order of the bits within bit-fields.

**Example:**
```
typedef struct
{
uint16_t count; // offset 0
uint16_t max_count; // offset 2
uint16_t _unused; // offset 4
uint16_t enable uint16_t b_interrupt : 1; // offset 6 bit
uint16_t _unused1 : 7; // offset 6 bits 12-6
uint16_t b_complete : 1; // offset 6 bit
uint16_t _unused2 : 4; // offset 6 bits 4-1
uint16_t b_periodic : 1; // offset 6 bit
} timer_reg_t;
// Preprocessor check of timer register layout byte count.
#if ((8 != sizeof(timer_reg_t))
#
error “timer_reg_t struct size incorrect (expected 8 bytes)”
#endif
```

### Reasoning

 Owing to differences across processor families and loose definitions in
the ISO C language standards, there is a tremendous amount of implementation-
defined behavior in the area of structures and unions. Bit-fields, in particular, suffer
from severe portability problems, including the lack of a standard bit ordering and
no official support for the fixed-width integer types they so often call out to be used
with. The methods available to check the layout of such data structures include
static assertions or other compile-time checks as well as the use of preprocessor
directives, e.g., to select one of two competing struct layouts based on the compiler.


## Booleans

### Rules

- Boolean variables shall be declared as type bool .

- Non-Boolean values shall be converted to Boolean via use of relational
operators (e.g., < or != ), not via casts.

**Example:**
```
#include <stdbool.h>
...
bool b_in_motion = (0 != speed_in_mph);
```
### Reasoning

The C90 standard did not define a data type for Boolean variables and C
programmers have widely treated any non-zero integer value as true. The C99
language standard is backward compatible with this old style, but also introduced a
new data type for Boolean variables along with new constants true and false in the
stdbool.h header file.

# Procedure Rules

## Naming Conventions

### Rules

- No procedure shall have a name that is a keyword of any standard
version of the C or C++ programming language. Restricted names
include interrupt , inline , class , true , false , public , private ,
friend , protected , and many others.

- No procedure shall have a name that overlaps a function in the C
Standard Library. Examples of such names include strlen , atoi ,
and memset .

- No procedure shall have a name that begins with an underscore.

- No procedure name shall be longer than 31 characters.

- No function name shall contain any uppercase letters.

- No macro name shall contain any lowercase letters.

-  Underscores shall be used to separate words in procedure names.

- Each procedure’s name shall be descriptive of its purpose. Note that
procedures encapsulate the “actions” of a program and thus benefit from the
use of verbs in their names (e.g., adc_read() ); this “noun-verb” word
ordering is recommended. Alternatively, procedures may be named
according to the question they answer (e.g., led_is_on() ).

- The names of all public functions shall be prefixed with their module name
and an underscore (e.g., sensor_read() ).

### Reasoning

 Good function names make reviewing and maintaining code easier (and
thus cheaper). The data (variables) in programs are nouns. Functions manipulate
data and are thus verbs. The use of module prefixes is in keeping with the
important goal of encapsulation and helps avoid procedure name overlaps.

## Functions

### Rules

- All reasonable effort shall be taken to keep the length of each function limited
to one printed page, or a maximum of 100 lines.

- Whenever possible, all functions shall be made to start at the top of a printed
page, except when several small functions can fit onto a single page.

- It is a preferred practice that all functions shall have just one exit point and it
shall be via a return at the bottom of the function.

- A prototype shall be declared for each public function in the module
header file.

- All private functions shall be declared static .

- Each parameter shall be explicitly declared and meaningfully named.

One way this can be accomplished is to insert a form feed character ‘FF’ (ASCII 0x0C) at the
beginning of the first line on the comment block that precedes the function definition.

**Example:**
```
int
state_change (int event)
{
int result = ERROR;
if (EVENT_A == event)
{
result = STATE_A;
}
else
{
result = STATE_B;
}
return (result);
}
```

### Reasoning

Code reviews take place at the function level and often on paper. Each
function should thus ideally be visible on a single printed page, so that flipping
papers back and forth do not distract the reviewers.
Multiple return statements should be used only when it improves the
readability of the code.

## Function-Like Macros

### Rules

- Parameterized macros shall not be used if a function can be written
to accomplish the same behavior.

- If parameterized macros are used for some reason, these rules apply:
  - Surround the entire macro body with parentheses.
  - surround each use of a parameter with parentheses.
  - Use each parameter no more than once, to avoid unintended
side effects.
  - Never include a transfer of control (e.g., return keyword).

**Example:**

```
// Don’t do this ...
#define MAX(A, B)
((A) > (B) ? (A) : (B))
// ... when you can do this instead.
inline int max(int num1, int num2)
```

### Reasoning

There are a lot of risks associated with the use of preprocessor defines,
and many of them relate to the creation of parameterized macros. The extensive use
of parentheses (as shown in the example) is important, but does not eliminate the
unintended double increment possibility of a call such as MAX(i++, j++) . Other
risks of macro misuse include comparison of signed and unsigned data or any test of
floating-point data. Making matters worse, macros are invisible at run-time and
thus impossible to step into within the debugger.

## Threads of Execution

### Rules

All functions that encapsulate threads of execution (a.k.a., tasks, processes)
shall be given names ending with “ _thread ” (or “ _task ”, “ _process ”).

**Example:**
```
void
alarm_thread (void * p_data)
{
alarm_t alarm = ALARM_NONE;
int err
= OS_NO_ERR;
for (;;)
{
alarm = OSMboxPend(alarm_mbox, &err);
// Process alarm here.
}}
```
### Reasoning

Each task in a real-time operating system (RTOS) is like a mini- main() ,
typically running forever in an infinite loop. It is valuable to easily identify these
important, asynchronous functions during code reviews and debugging sessions.
Enforcement: This rule shall be followed during the detailed design phase and
enforced during code reviews.

## Interrupt Service Routines

### Rules

- Interrupt service routines (ISRs) are not ordinary functions. The compiler
must be informed that the function is an ISR by way of a #pragma or
compiler-specific keyword, such as “__interrupt ”.

- All functions that implement ISRs shall be given names ending with “ _isr ”.

- To ensure that ISRs are not inadvertently called from other parts of the
software (they may corrupt the CPU and call stack if this happens),
each ISR function shall be declared static and/or be located at the
end of the associated driver module as permitted by the target platform.

- A stub or default ISR shall be installed in the vector table at the location of all
unexpected or otherwise unhandled interrupt sources. Each such stub could
attempt to disable future interrupts of the same type, say at the interrupt
controller, and assert() .

**Example:**
```
#pragma irq_entry
void
timer_isr (void)
{
uint8_t static prev = 0x00; // prev button states
uint8_t curr = *gp_button_reg; // curr button states
// Compare current and previous button states.
g_debounced |= (prev & curr); // record all closes
g_debounced &= (prev | curr); // record all opens
// Save current pin states for next interrupt
prev = curr;
// Acknowledge timer interrupt at hardware, if necessary.
}
```

### Reasoning

An ISR is an extension of the hardware. By definition, it and the
straight-line code is asynchronous to each other. If they share global variables or
registers, those singleton objects must be protected via interrupt disabled in the
straight-line code. The ISR must not get hung up inside the operating system or
waiting for a variable or register to change value.
Note that platform-specific ISR installation steps vary and may require ISRs
functions to have prototypes and in other ways be visible to at least one other
function.

Although stub interrupt handlers don’t directly prevent defects, they can
certainly make a system more robust in real-world operating conditions.

Enforcement: These rules shall be enforced during code reviews.

# Variable Rules

## Naming Conventions

### Rules

- No variable shall have a name that is a keyword of C, C++, or any
other well-known extension of the C programming language,
including specifically K&R C and C99. Restricted names include
interrupt , inline , restrict , class , true , false , public , private ,
friend , and protected .

- No variable shall have a name that overlaps with a variable name
from the C Standard Library (e.g., errno ).

- No variable shall have a name that begins with an underscore.

- No variable name shall be longer than 31 characters.

- No variable name shall be shorter than 3 characters, including loop counters.

- No variable name shall contain any uppercase letters.

- No variable name shall contain any numeric value that is called out
elsewhere, such as the number of elements in an array or the number of bits
in the underlying type.

- Underscores shall be used to separate words in variable names.

- Each variable’s name shall be descriptive of its purpose.

- The names of any global variables shall begin with the letter ‘ g ’.
For example, g_zero_offset .

- The names of any pointer variables shall begin with the letter ‘ p ’.
For example, p_led_reg .

- The names of any pointer-to-pointer variables shall begin with the letters ‘ pp ’.
For example, pp_vector_table .

- The names of all integer variables containing Boolean information
(including 0 vs. non-zero) shall begin with the letter ‘ b ’ and phrased
as the question they answer. For example, b_done_yet or
b_is_buffer_full .

-  The names of any variables representing non-pointer handles for objects, e.g.,
file handles, shall begin with the letter ‘ h ’. For example, h_input_file .

- In the case of a variable name requiring multiple of the above prefixes, the
order of their inclusion before the first underscore shall be [ g ][ p | pp ][ b | h ].

### Reasoning

The base rules are adopted to maximize code portability across
compilers. Many C compilers recognize differences only in the first 31 characters in
a variable’s name and reserve names beginning with an underscore for internal
names.

The other rules are meant to highlight risks and ensure consistent proper use of
variables. For example, all code relating to the use of global variables and other
singleton objects, including peripheral registers, needs to be carefully considered to
ensure there can be no race conditions or data corruptions via asynchronous writes.
Enforcement: These rules shall be enforced during code reviews.

## Initialization

### Rules

- All variables shall be initialized before use.

-  It is preferable to define local variables as you need them, rather than all at
the top of a function.

- If project- or file-global variables are used, their definitions shall be grouped
together and placed at the top of a source code file.

- Any pointer variable lacking an initial address shall be initialized to NULL .

**Example:**
```
uint32_t
g_array[NUM_ROWS][NUM_COLS] = { ... };
...
for (int col = 0; col < NUM_COLS; col++)
{
g_array[row][col] = ...;
}
```

### Reasoning

Too many programmers assume the C run-time will watch out for them,
e.g., by zeroing the value of uninitialized variables on system startup. This is a bad
assumption, which can prove dangerous in a mission-critical system. For readability
reasons it is better to declare local variables as close as possible to their first use,
which C99 makes possible by incorporating that earlier feature of C++.
Enforcement: An automated tool shall scan all of the source code prior to each build,
to warn about variables used prior to initialization; static analysis tools can do this.
The remainder of these rules shall be enforced during code reviews.
[Uwano] describes back-and-forth code review eye movements that demonstrate the value
of placing variable declarations as close as possible to the code that first references them.

# Statement Rules

## Variable Declarations

### Rules

The comma operator ( , ) shall not be used within variable
declarations.

**Example:**

```
char * x, y;
// Was y intended to be a pointer also?
Don’t do this.
```

### Reasoning

The cost of placing each declaration on a line of its own is low. By
contrast, the risk that either the compiler or a maintainer will misunderstand your
intentions are high.

## Conditional Statements

### Rules

- It is a preferred practice that the shortest (measured in lines of code) of the if
and else if clauses should be placed first.

-  Nested if...else statements shall not be deeper than two levels. Use function
calls or switch statements to reduce complexity and aid understanding.

- Assignments shall not be made within an if or else if test.

-  Any if statement with an else if clause shall end with an else clause.

**Example:**

```
if (NULL == p_object)
{
result = ERR_NULL_PTR;
}
else if (p_object = malloc(sizeof(object_t))) // No assignments!
{
...
}
else
{
// Normal processing steps, which require many lines of code.
...
}
```

### Reasoning

Long clauses can distract the human eye from the decision-path logic.
By putting the shorter clause earlier, the decision path becomes easier to follow.
(And easier to follow is always good for reducing bugs.) Deeply nested if...else
statements are a sure sign of a complex and fragile state machine implementation;
there is always a safer and more readable way to do the same thing.
Enforcement: These rules shall be enforced during code reviews.

## Switch Statements

### Rules

- The break for each case shall be indented to align with the
associated case , rather than with the contents of the case code block.

- All switch statements shall contain a default block.

-  Any case designed to fall through to the next shall be commented to clearly
explain the absence of the corresponding break .

**Example:**
```
switch (err)
{
case ERR_A:
...
break;
case ERR_B:
...
// Also perform the steps for ERR_C.
case ERR_C:
...
break;
default:
...
break;
}
```

### Reasoning

C’s switch statements are powerful constructs, but prone to errors such
as omitted break statements and unhandled cases. By aligning the case labels with
Their break statements make it easier to spot a missing break .

## Loops

### Rules

- Magic numbers shall not be used as the initial value or in the endpoint test of
a while , do...while , or for loop.

- With the exception of the initialization of a loop counter in the first clause of a
for statement and the change to the same variable in the third, no assignment
shall be made in any loop’s controlling expression.

- Infinite loops shall be implemented via controlling expression for (;;) .

- Each loop with an empty body shall feature a set of braces enclosing a
comment to explain why nothing needs to be done until after the loop
terminates.

**Example:**

```
// Why would anyone bury a magic number (e.g., “100”) in their code?
for (int row = 0; row < 100; row++)
{
// Descriptively-named constants prevent defects and aid readability.
for (int col = 0; col < NUM_COLS; col++)
{
...
}
```

### Reasoning

It is always important to synchronize the number of loop iterations to
the size of the underlying data structure. Doing this via a descriptively-named
constant prevents defects that result when changes in one part of the code, such as
the dimension of an array, are not matched in other areas of the code.
Enforcement: These rules shall be enforced during code reviews.

Note that the sizeof macro is a theoretically handy way to dimension an array but that
this method does not work when you pass a pointer to the array instead of the array itself.

## Jumps

### Rules

- The use of goto statements shall be restricted as per Rule 1.7.c.

- C Standard Library functions abort() , exit() , setjmp() , and longjmp()
shall not be used.

### Reasoning

Algorithms that utilize jumps to move the instruction pointer can and
should be rewritten in a manner that is more readable and thus easier to maintain.
Enforcement: These rules shall be enforced by an automated scan of all modified or
new modules for inappropriate use of forbidden tokens. To the extent that the use of
goto is permitted, code reviewers should investigate alternative code structures to
improve code maintainability and readability.

## Equivalence Tests

### Rules

When evaluating the equality of a variable against a constant, the
constant shall always be placed to the left of the equal-to operator
( == ).

**Example:**

```
if (NULL == p_object)
{
return (ERR_NULL_PTR);
}
```
### Reasoning

 It is always desirable to detect possible typos and as many other coding
defects as possible at compile-time. Defect discovery in later phases is not
guaranteed and often also more costly. By following this rule, any compiler will
reliably detect erroneous attempts to assign (i.e., = instead of == ) a new value to a
constant.












 









